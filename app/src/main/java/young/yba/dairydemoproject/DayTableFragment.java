package young.yba.dairydemoproject;

import android.support.v4.app.Fragment;
import android.view.View;
import android.view.ViewGroup;
import android.view.LayoutInflater;
import android.os.Bundle;
import android.widget.TextView;

/**
 * Created by Mr Smith on 10.08.2015.
 */
public class DayTableFragment extends Fragment {
    public static DayTableFragment newInstance()
    {
        DayTableFragment fragment = new DayTableFragment();
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_day_table, parent, false);

        return view;
    }
}
