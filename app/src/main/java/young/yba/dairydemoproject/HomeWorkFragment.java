package young.yba.dairydemoproject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.UUID;

/**
 * Created by Mr Smith on 10.08.2015.
 */
public class HomeWorkFragment extends Fragment {
   private TextView mScrollDay;
    public static HomeWorkFragment newInstance()
    {
        HomeWorkFragment fragment = new HomeWorkFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_home_work, parent, false);
        mScrollDay = (TextView)view.findViewById(R.id.scrollDay_textView);
        //to communicate mResentDay with server --> next code line
        mScrollDay.setText(R.string.monday_day);


        return view;
    }
}
