package young.yba.dairydemoproject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;

/**
 * Created by Mr Smith on 10.08.2015.
 */
public class HomeWorkPagerActivity extends FragmentActivity {
    private ViewPager mViewPager;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mViewPager = new ViewPager(this);
        mViewPager.setId(R.id.viewPager);
        setContentView(mViewPager);

        FragmentManager fragmentManager = getSupportFragmentManager();
        mViewPager.setAdapter(new FragmentPagerAdapter(fragmentManager) {
            @Override
            public Fragment getItem(int position) {

                Fragment ret = null;

                switch (position) {
                    case 0:
                        ret = DayTableFragment.newInstance();
                        break;
                    case 1:
                        ret = HomeWorkFragment.newInstance();
                        break;
                    case 2:
                        ret = TableFragment.newInstance();
                        break;
                }
                return ret;
            }
            @Override
            public int getCount() {
                return 3;
            }
        });

        mViewPager.setCurrentItem(1);
    }
}