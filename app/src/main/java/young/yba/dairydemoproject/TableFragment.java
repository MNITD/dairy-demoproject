package young.yba.dairydemoproject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Mr Smith on 10.08.2015.
 */
public class TableFragment extends Fragment {

    public static TableFragment newInstance()
    {
        TableFragment fragment = new TableFragment();
        return fragment;
    }
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.fragment_table, parent, false);

        return view;
    }
}
